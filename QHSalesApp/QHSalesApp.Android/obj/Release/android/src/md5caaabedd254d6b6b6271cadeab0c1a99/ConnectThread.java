package md5caaabedd254d6b6b6271cadeab0c1a99;


public class ConnectThread
	extends java.lang.Thread
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_run:()V:GetRunHandler\n" +
			"";
		mono.android.Runtime.register ("PCLBluetooth.Android.ConnectThread, PCLBluetooth.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ConnectThread.class, __md_methods);
	}


	public ConnectThread () throws java.lang.Throwable
	{
		super ();
		if (getClass () == ConnectThread.class)
			mono.android.TypeManager.Activate ("PCLBluetooth.Android.ConnectThread, PCLBluetooth.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public ConnectThread (android.bluetooth.BluetoothDevice p0, int p1) throws java.lang.Throwable
	{
		super ();
		if (getClass () == ConnectThread.class)
			mono.android.TypeManager.Activate ("PCLBluetooth.Android.ConnectThread, PCLBluetooth.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Bluetooth.BluetoothDevice, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1 });
	}


	public void run ()
	{
		n_run ();
	}

	private native void n_run ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
