package md57d3d8062e3e9a64ad1430263be8244a6;


public class CountingRequestBody_CountingSink
	extends okio.ForwardingSink
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_write:(Lokio/Buffer;J)V:GetWrite_Lokio_Buffer_JHandler\n" +
			"";
		mono.android.Runtime.register ("Plugin.FileUploader.CountingRequestBody+CountingSink, Plugin.FileUploader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CountingRequestBody_CountingSink.class, __md_methods);
	}


	public CountingRequestBody_CountingSink (okio.Sink p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == CountingRequestBody_CountingSink.class)
			mono.android.TypeManager.Activate ("Plugin.FileUploader.CountingRequestBody+CountingSink, Plugin.FileUploader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "OkHttp.Okio.ISink, OkHttp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", this, new java.lang.Object[] { p0 });
	}


	public void write (okio.Buffer p0, long p1)
	{
		n_write (p0, p1);
	}

	private native void n_write (okio.Buffer p0, long p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
