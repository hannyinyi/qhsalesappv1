﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Data;
using System.Reflection;
using QHSalesApp;

namespace QHSalesApp.Droid
{
    public class SoapService : ISoapService
    {
        public AppSvc.WebService service { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public SoapService()
        {
            GetNavUrl();
        }

        private void GetNavUrl()
        {
            //if(service!=null) service.Dispose();
            service = new AppSvc.WebService();
            string setting_url = "";
            try
            {
                setting_url = Helpers.Settings.GeneralSettings;
            }
            catch (System.NullReferenceException) { }
            catch (System.Collections.Generic.KeyNotFoundException) { }

            if (setting_url == "")
            {
                service.Url = Constants.SoapUrl; //dummy add for avoid nullrefexception 
            }
            else
            {
                service.Url = setting_url;
            }
            // service.Credentials = new NetworkCredential("admin", "bingo28*", "dptech");
        }

        public string DeviceRegistration(string strMobileAccessKey, string deviceId)
        {
            try
            {
                service.Dispose();
                GetNavUrl();
                return service.RegisterDevice(strMobileAccessKey, deviceId);
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }

        }

        public string UserLogin(string mobileAccessKey, string deviceId,string userEmail,string password)
        {
            try
            {
                service.Dispose();
                GetNavUrl();
                return service.UserLogin(mobileAccessKey, deviceId, userEmail, password);
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public DataTable GetLoginUsers(string mobileAccessKey)
        {
            DataTable dt = new DataTable();
            service.Dispose();
            GetNavUrl();
            return service.GetMobileUsers(mobileAccessKey);
        }

        public DataTable GetItems()
        {
            DataTable dt = new DataTable();
            service.Dispose();
            GetNavUrl();
            return service.GetQHItems();
        }

        public DataTable GetItemUOMs()
        {
            DataTable dt = new DataTable();
            service.Dispose();
            GetNavUrl();
            return service.GetQHItemUOMs();
        }

        public DataTable GetItemSalesPrices(string salesPersonCode)
        {
            DataTable dt = new DataTable();
            service.Dispose();
            GetNavUrl();
            return service.GetQHItemPrices(salesPersonCode);
        }

        public DataTable GetCustomers(string salesPersonCode)
        {
            DataTable dt = new DataTable();
            service.Dispose();
            GetNavUrl();
            return service.GetQHCustomers(salesPersonCode);
        }

        public DataTable GetCustomerPriceHistory(string custno,string itemno)
        {
            DataTable dt = new DataTable();
            service.Dispose();
            GetNavUrl();
            return service.GetQHCustomerPriceHistory(custno,itemno);
        }

        public DataTable GetCustLedgerEntry(string custno)
        {
            DataTable dt = new DataTable();
            service.Dispose();
            GetNavUrl();
            return service.GetQHCustLedgerEntry(custno);
        }

        public DataTable GetPaymentMethods()
        {
            DataTable dt = new DataTable();
            service.Dispose();
            GetNavUrl();
            return service.GetQHPaymentMethods();
        }

        public string ExportSalesHeader(string docNo, string selltocustomer, string selltoName, string billtoCustomer, string billtoName, string docDate, string status, string paymentMethod, decimal totalAmt,string doctype,string note,string strSingature)
        {
            service.Dispose();
            GetNavUrl();
            return service.ExportQHSalesOrder(docNo,selltocustomer,selltoName,billtoCustomer,billtoName,docDate,status,paymentMethod,totalAmt,doctype,note,strSingature);
        }

        public string ExportSalesLine(string docNo, string itemNo, string locCode, decimal qty, decimal focQty, string uom, decimal unitPrice, decimal lineDisPercent, decimal lineDisAmt, decimal lineAmt)
        {
                service.Dispose();
                GetNavUrl();
                return service.ExportQHSalesLine(docNo, itemNo, locCode, qty, focQty, uom, unitPrice, lineDisPercent, lineDisAmt, lineAmt);
        }


        public string ExportPayment(string docNo, string onDate, string customerNo, string paymentMethod, decimal amount, string strSignature, string strImage, string salesPersonCode, string note, string recStatus,string refdocno,string sourcetype)
        {
            service.Dispose();
            GetNavUrl();
            return service.ExportQHPayment(docNo, onDate, customerNo, paymentMethod, amount, strSignature, strImage, salesPersonCode, note, recStatus,refdocno,sourcetype);
        }

        public  DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
    }
}