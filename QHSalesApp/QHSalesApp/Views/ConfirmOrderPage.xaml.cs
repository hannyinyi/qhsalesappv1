﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SignaturePad.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using Acr.UserDialogs;

namespace QHSalesApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfirmOrderPage : ContentPage
    {
        SalesHeader head { get; set; }
        Customer customer { get; set; }
        string DocumentNo { get; set; }
        string DocumentDate { get; set; }
        string pagefrom { get; set; }
        int EntryNo { get; set; }
        ObservableCollection<SalesLine> Records { get; set; }
        List<PaymentMethod> paymethods { get; set; }
        decimal TotalAmount { get; set; }
        public ConfirmOrderPage(string docno,ObservableCollection<SalesLine> records,string from)
        {
            InitializeComponent();
            DocumentNo = docno;
            this.Title = App.gPageTitle;
            Records = new ObservableCollection<SalesLine>();
            pagefrom = from;
            Records = records;
        }


        protected async override void OnAppearing()
        {
            base.OnAppearing();
            DocNoLabel.Text = DocumentNo;
            //LoadPaymentMethods();
            TotalAmount = Records.Sum(x => x.LineAmount);
            TotalAmountLabel.Text = string.Format("Total : {0:0.00}",TotalAmount);//"Total : " + TotalAmount.ToString();
            // Get header info 
            head = new SalesHeader();
            DataManager manager = new DataManager();
            head =await manager.GetSalesHeaderbyDocNo(DocumentNo);

            
            customer = new Customer();
            customer = await manager.GetCustomerbyCode(head.BillToCustomer);

            if(customer!=null)
            {
                BillToNameLabel.Text = customer.Name;
                Line1Label.Text = customer.Address + "," + customer.Address2 + "," + customer.City + "  " + customer.Postcode; ;
                Line2Label.Text = customer.PhoneNo + "," + customer.MobileNo;
               
                //Line4Label.Text = string.Empty;
            }
            if (pagefrom == "Released")
            {
                ConfirmButton.IsVisible = false;
                CustomerSignaturePad.IsVisible = false;
                imgSignature.IsVisible = true;
                //PaymentMethodsPicker.IsEnabled = false;
                //PaymentMethodsPicker.SelectedItem = head.PaymentMethod;

                imgSignature.Source = Xamarin.Forms.ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(head.StrSignature)));
               // imgSignature.Source = head.StrSignature;
                //CustomerSignaturePad.
            }
            else
            {
                CustomerSignaturePad.IsVisible = true;
                imgSignature.IsVisible = false;
            }
        }

     

        private void Clear_Button_OnClicked(object sender, EventArgs e)
        {
            CustomerSignaturePad.Clear();
        }

        private async void ConfirmButton_OnClicked(object sender, EventArgs e)
        {
            string msg = string.Empty;
            if (App.gDocType == "SO")
                msg = "Are you sure to release Sales Order?";
            else
                msg = "Are you sure to release Credit Memo?";
            var answer = await DisplayAlert("Confirm",msg, "Yes", "No");
            if (answer)
            {
                //var signedImageStream = await CustomerSignaturePad.GetImageStreamAsync(SignatureImageFormat.Png);
                //var signatureMemoryStream = signedImageStream as MemoryStream;
                //byte[] bytes = signatureMemoryStream.ToArray();
                //string _signature64str = Convert.ToBase64String(bytes);

                string _signature64str = string.Empty;
                try
                {
                    var signedImageStream = await CustomerSignaturePad.GetImageStreamAsync(SignatureImageFormat.Png);
                    if (signedImageStream is MemoryStream)
                    {
                        var signatureMemoryStream = signedImageStream as MemoryStream;
                        byte[] bytes = signatureMemoryStream.ToArray();
                        _signature64str = Convert.ToBase64String(bytes);
                    }
                }
                catch (ArgumentException)
                {
                    _signature64str = string.Empty;
                }

                DataManager manager = new DataManager();
                string retval = await manager.SaveSQLite_SalesHeader(new SalesHeader
                {
                    ID = head.ID,
                    DocumentNo = head.DocumentNo,
                    DocumentDate = head.DocumentDate,
                    BillToCustomer = head.BillToCustomer,
                    BillToName = head.BillToName,
                    SellToCustomer = head.SellToCustomer,
                    SellToName = head.SellToName,
                    TotalAmount = head.TotalAmount,
                    Status = "Released",
                    PaymentMethod = string.Empty,
                    StrSignature = _signature64str,
                    DocumentType = head.DocumentType,
                    Note = head.Note
                });

                if (retval == "Success")
                {
                    //DependencyService.Get<IMessage>().LongAlert(retval);
                    UserDialogs.Instance.ShowSuccess(retval, 3000);
                    //var navipages = Navigation.NavigationStack.ToList();
                    //foreach(var pg in navipages)
                    //{
                    //    Navigation.RemovePage(pg);
                    //}
                    App.gSOStatus = "Released";
                    Navigation.PushAsync(new MainPage(2));
                }
                else
                {
                   // DependencyService.Get<IMessage>().LongAlert(retval);
                    UserDialogs.Instance.ShowError(retval, 3000);
                }

              
            }
                
        }
    }
}