﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QHSalesApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SalesOrderEntryPage : ContentPage
    {
        readonly Database database;
        private SalesHeader data { get; set; }
        private int EntryNo { get; set; }
        private string DocNo { get; set; }
        private string DocType { get; set; }
        private decimal invTotal { get; set; }
        private string CurStatus { get; set; }
        private string paraButton { get; set; }
        private ObservableCollection<SalesHeader> records { get; set; }
        //private bool IsNewEntry { get; set; }
        private bool IsBack { get; set; }
        //  bool hasClicked { get; set; }
        //  Action<object, EventArgs> _setClick;
        private bool _isloading;
        
        public bool IsLoading
        {
            get { return this._isloading; }
            set
            {
                this._isloading = value;
                OnPropertyChanged("IsLoading");
            }
        }

        public SalesOrderEntryPage(int entryNo)
        {
            InitializeComponent();
            database = new Database(Constants.DatabaseName);
            database.CreateTable<SalesHeader>();
            Title = App.gPageTitle;
            EntryNo = entryNo;
            SellToCustomerEntry.Completed += SellToCustomerEntry_Completed;
            SellToCustomerEntry.Unfocused += SellToCustomerEntry_Unfocused;
            SelltoCustomerlookUpButton.ButtonIcon = AwasomeIcon.FASearch;
            SelltoCustomerlookUpButton.ButtonColor = Color.FromHex("#EC2029");
            SelltoCustomerlookUpButton.OnTouchesEnded += lookUpButton_OnTouchesEnded;

            //BillToCustomerlookUpButton.ButtonIcon = AwasomeIcon.FASearch;
            //BillToCustomerlookUpButton.ButtonColor = Color.FromHex("#EC2029");
            //BillToCustomerlookUpButton.OnTouchesEnded += lookUpButton_OnTouchesEnded;

            this.BackgroundColor = Color.FromHex("#dddddd");
            saveButton.Clicked += SaveButton_Clicked;
            IsBack = false;
            IsLoading = false;
            BindingContext = this;
        }

        private void SellToCustomerEntry_Unfocused(object sender, FocusEventArgs e)
        {
            if (!string.IsNullOrEmpty(SellToCustomerEntry.Text))
            {
                DataManager manager = new DataManager();
                Customer customer = new Customer();
                customer = manager.GetSQLite_CustomerbyCustNo(SellToCustomerEntry.Text);
                if (customer != null)
                {
                    SellToCustomerEntry.Text = customer.CustomerNo;
                    SellToNameLabel.Text = customer.Name;
                }
                else
                {
                    UserDialogs.Instance.ShowError("Wrong customer no or customer does not existed!", 3000);

                    SellToCustomerEntry.Text = string.Empty;
                    SellToNameLabel.Text = string.Empty;
                    SellToCustomerEntry.Focus();
                }
            } 
        }

        private void SellToCustomerEntry_Completed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(SellToCustomerEntry.Text))
            {
                DataManager manager = new DataManager();
                Customer customer = new Customer();
                customer = manager.GetSQLite_CustomerbyCustNo(SellToCustomerEntry.Text);
                if (customer != null)
                {
                    SellToCustomerEntry.Text = customer.CustomerNo;
                    SellToNameLabel.Text = customer.Name;
                }
                else
                {
                    UserDialogs.Instance.ShowError("Wrong customer no or customer does not existed!", 3000);

                    SellToCustomerEntry.Text = string.Empty;
                    SellToNameLabel.Text = string.Empty;
                    SellToCustomerEntry.Focus();
                }
            }
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            data = new SalesHeader();
            if (!IsBack)
            {
                UserDialogs.Instance.ShowLoading("Loading", MaskType.Black); //IsLoading = true;
                Task.Run(async () =>
                {
                    try
                    {
                        if (EntryNo!=0)
                        {
                            DataManager manager = new DataManager();
                            data = await manager.GetSalesHeaderbyID(EntryNo);
                        } 
                        else
                            data = null;

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayData(data);
                            UserDialogs.Instance.HideLoading(); //IsLoading = false;
                        });

                    }
                    catch (OperationCanceledException ex)
                    {
                        UserDialogs.Instance.HideLoading(); //IsLoading = false;
                        //DependencyService.Get<IMessage>().LongAlert(ex.Message.ToString());
                        UserDialogs.Instance.ShowError(ex.Message.ToString(), 3000);
                    }
                    catch (Exception ex)
                    {
                        UserDialogs.Instance.HideLoading(); //IsLoading = false;
                        //DependencyService.Get<IMessage>().LongAlert(ex.Message.ToString());
                        UserDialogs.Instance.ShowError(ex.Message.ToString(), 3000);
                    }
                });
            }
        }

        void DisplayData(SalesHeader record)
        {
            if (record != null)
            {
                    
                //InvoiceNoLabel.IsVisible = true;
                //InvoiceNoEntry.IsVisible = true;
                EntryNo = record.ID;
                InvoiceNoLabel.Text = record.DocumentNo;
                DocumentDatePicker.Date =Convert.ToDateTime(record.DocumentDate);
                SellToCustomerEntry.Text = record.SellToCustomer;
                SellToNameLabel.Text = record.SellToName;
                invTotal = record.TotalAmount;
                //BillToCustomerEntry.Text = record.BillToCustomer;
                //BillToNameLabel.Text = record.BillToName;
                //  StatusPicker.SelectedItem = record.Status;
                CurStatus = record.Status;
            // NoteEntry.Text = record.Note;
            //if (CurStatus == "Released")
            //    {
            //        StatusPicker.IsEnabled = false;
            //    }
            }
            else
            {
                EntryNo = 0;
                DataManager manager = new DataManager();
                if(App.gDocType=="SO")
                    InvoiceNoLabel.Text = manager.GetLastNoSeries(App.gSOPrefix); 
                else
                    InvoiceNoLabel.Text = manager.GetLastNoSeries(App.gCRPrefix);
                DocumentDatePicker.Date = DateTime.Today;
                invTotal = 0;
               // StatusPicker.SelectedItem = "Open";
                CurStatus = "Open";
                // StatusPicker.IsEnabled = false;
                // //Status: Open (same as in NAV), Released (same as in NAV), Completed (when Transfer Order is posted and deleted)
            }
        }

        private void lookUpButton_OnTouchesEnded(object sender, IEnumerable<NGraphics.Point> e)
        {
            var page = new CustomerPage();
            var obj = (ActionButton)sender;
            paraButton = obj.CommandParameter.ToString();
            page.listview.ItemSelected += Listview_ItemSelected;
            Navigation.PushAsync(page);
        }

        private void Listview_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;
            IsBack = true;
            var selectedItem = e.SelectedItem as Customer;
            if (paraButton == "SelltoCustomer")
            {
                SellToCustomerEntry.Text = selectedItem.CustomerNo;
                SellToNameLabel.Text = selectedItem.Name;
                //BillToCustomerEntry.Text = selectedItem.CustomerNo;
                //BillToNameLabel.Text = selectedItem.Name;
            } 
            else
            {
                //BillToCustomerEntry.Text = selectedItem.CustomerNo;
                //BillToNameLabel.Text = selectedItem.Name;
            }
                
            Navigation.PopAsync();
        }

        private async void SaveButton_Clicked(object sender, EventArgs e)
        {
            //if(!hasClicked)
            //{
            //_setClick(sender, e);
            //hasClicked = true;

            if (CurStatus == "Open")
            {
                if(!string.IsNullOrEmpty(SellToCustomerEntry.Text))
                {
                    DataManager manager = new DataManager();
                    string retval = await manager.SaveSQLite_SalesHeader(new SalesHeader
                    {
                        ID = EntryNo,
                        DocumentNo = InvoiceNoLabel.Text,
                        DocumentDate = DocumentDatePicker.Date.ToString("yyyy-MM-dd"),
                        BillToCustomer = SellToCustomerEntry.Text,
                        BillToName = SellToCustomerEntry.Text,
                        SellToCustomer = SellToCustomerEntry.Text,
                        SellToName = SellToNameLabel.Text,
                        Status = "Open",
                        TotalAmount = invTotal,
                        DocumentType = App.gDocType,
                        Note = string.Empty
                    });

                    if (retval == "Success")
                    {
                        if (App.gDocType == "SO")
                            manager.IncreaseNoSeries(App.gSOPrefix, InvoiceNoLabel.Text);
                        else
                            manager.IncreaseNoSeries(App.gCRPrefix, InvoiceNoLabel.Text);
                        //DependencyService.Get<IMessage>().LongAlert(retval);
                        UserDialogs.Instance.ShowSuccess(retval, 3000);
                        Navigation.PopAsync();
                    }
                    else
                    {
                        //DependencyService.Get<IMessage>().LongAlert(retval);
                        UserDialogs.Instance.ShowError(retval, 3000);
                    }
                }
                else
                {
                    UserDialogs.Instance.ShowError("Required Sell to Customer!", 3000);
                    SellToCustomerEntry.Focus();
                }
                
            }
            else
            {
                //DependencyService.Get<IMessage>().LongAlert("Not allow to save released Sales Order!");
                UserDialogs.Instance.ShowError("Not allow to save released Sales Order!", 3000);
            }
            // }
        }
    }
}