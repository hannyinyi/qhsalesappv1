﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QHSalesApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemEntryPage : ContentPage
    {
        readonly Database database;
        private SalesLine data { get; set; }
        private int EntryNo { get; set; }
        private string DocNo { get; set; }
        private string DocType { get; set; }
        private int cmdPara { get; set; }
        private decimal oldLineAmt { get; set; }
        private ObservableCollection<SalesLine> records { get; set; }
        private bool isItemExisted { get; set; }
        private bool IsBack { get; set; }
        private bool _isloading;
        public bool IsLoading
        {
            get { return this._isloading; }
            set
            {
                this._isloading = value;
                OnPropertyChanged("IsLoading");
            }
        }
        public ItemEntryPage(int id, string docno)
        {
            InitializeComponent();
            Title = App.gPageTitle;
            database = new Database(Constants.DatabaseName);
            database.CreateTable<SalesLine>();
            EntryNo = id;
            DocNo = docno;
            ItemNoEntry.Completed += ItemNoEntry_Completed;
            ItemNoEntry.Unfocused += ItemNoEntry_Unfocused;
            ItemNolookUpButton.ButtonIcon = AwasomeIcon.FASearch;
            ItemNolookUpButton.ButtonColor = Color.FromHex("#EC2029");
            ItemNolookUpButton.OnTouchesEnded += ItemlookUpButton_OnTouchesEnded;

            UnitPricelookUpButton.ButtonIcon = AwasomeIcon.FASearch;
            UnitPricelookUpButton.ButtonColor = Color.FromHex("#EC2029");
            UnitPricelookUpButton.OnTouchesEnded += UnitPricelookUpButton_OnTouchesEnded; ;

            QuantityEntry.Completed += QuantityEntry_Completed;
            QuantityEntry.Unfocused += QuantityEntry_Unfocused;
            UnitPriceEntry.Completed += UnitPriceEntry_Completed;
            UnitPriceEntry.Unfocused += UnitPriceEntry_Unfocused;
           // FOCQtyEntry.Completed += FOCQtyEntry_Completed;
            
            //UomlookUpButton.ButtonIcon = AwasomeIcon.FASearch;
            //UomlookUpButton.ButtonColor = Color.FromHex("#EC2029");
            //UomlookUpButton.OnTouchesEnded += UomlookUpButton_OnTouchesEnded;

            this.BackgroundColor = Color.FromHex("#dddddd");
            saveButton.Clicked += SaveButton_Clicked;
            DeleteButton.Clicked += DeleteButton_Clicked;
            IsBack = false;
            IsLoading = false;
            BindingContext = this;
        }

        private async void ItemNoEntry_Unfocused(object sender, FocusEventArgs e)
        {
            if (!string.IsNullOrEmpty(ItemNoEntry.Text))
            {
                DataManager manager = new DataManager();
                Item item = new Item();
                item = await manager.GetSQLite_ItembyItemNo(ItemNoEntry.Text);
                if (item != null)
                {
                    ItemNoEntry.Text = item.ItemNo;
                    DescEntry.Text = item.Description;
                    UomEntry.Text = item.BaseUOM;
                    LoadDefaultPrice(item.ItemNo, App.gCustCode);
                    isItemExisted = true;
                    QuantityEntry.Focus();
                }
                else
                {
                    UserDialogs.Instance.ShowError("Wrong Item No or Item does not existed!", 3000);

                    ItemNoEntry.Text = string.Empty;
                    DescEntry.Text = string.Empty;
                    isItemExisted = false;
                    ItemNoEntry.Focus();
                }
            }
        }

        private async void ItemNoEntry_Completed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ItemNoEntry.Text))
            {
                DataManager manager = new DataManager();
                Item item = new Item();
                item = await manager.GetSQLite_ItembyItemNo(ItemNoEntry.Text);
                if (item !=null)
                {
                    ItemNoEntry.Text = item.ItemNo;
                    DescEntry.Text = item.Description;
                    UomEntry.Text = item.BaseUOM;
                    LoadDefaultPrice(item.ItemNo, App.gCustCode);
                    isItemExisted = true;
                    QuantityEntry.Focus();
                }
                else
                {
                    UserDialogs.Instance.ShowError("Wrong Item No or Item does not existed!", 3000);
                   
                    ItemNoEntry.Text = string.Empty;
                    DescEntry.Text = string.Empty;
                   isItemExisted = false;
                    ItemNoEntry.Focus();
                }
            }
        }

        private void QuantityEntry_Unfocused(object sender, FocusEventArgs e)
        {
            if (string.IsNullOrEmpty(QuantityEntry.Text))
            {
                QuantityEntry.Focus();
                UserDialogs.Instance.ShowError("Not allow blank  quantity!", 3000);
              //  DependencyService.Get<IMessage>().LongAlert("Not allow blank  quantity!");
                return;
            }
            //if (decimal.Parse(QuantityEntry.Text) == 0)
            //{
            //    //DependencyService.Get<IMessage>().LongAlert("Not allow 0 quantity!");
            //    UserDialogs.Instance.ShowError("Not allow 0 quantity!", 3000);
            //    QuantityEntry.Focus();
            //    return;
            //}

            if (!string.IsNullOrEmpty(UnitPriceEntry.Text))
            {
                //if (decimal.Parse(UnitPriceEntry.Text) == 0)
                //{
                //    //DependencyService.Get<IMessage>().LongAlert("Not allow 0 Unit Price!");
                //    UserDialogs.Instance.ShowError("Not allow Unit Price is 0!", 3000);
                //    UnitPriceEntry.Focus();
                //    return;
                //}

                UnitPriceEntry.Text = string.Format("{0:#,##0.00}", decimal.Parse(UnitPriceEntry.Text));
                ItemtotalLabel.Text = string.Format("{0:#,##0.00}", decimal.Parse(UnitPriceEntry.Text) * decimal.Parse(QuantityEntry.Text));
                saveButton.Focus();
            }
            else
            {
                //DependencyService.Get<IMessage>().LongAlert("Empty Unit Price!");
                UserDialogs.Instance.ShowError("Empty Unit Price!", 3000);
                this.Focus();
            }
        }

        private void UnitPriceEntry_Unfocused(object sender, FocusEventArgs e)
        {
            if (string.IsNullOrEmpty(QuantityEntry.Text))
            {
                QuantityEntry.Focus();
                //DependencyService.Get<IMessage>().LongAlert("Not allow blank  quantity!");
                UserDialogs.Instance.ShowError("Not allow blank  quantity!", 3000);
                return;
            }
            //if (decimal.Parse(QuantityEntry.Text) == 0)
            //{
            //    //DependencyService.Get<IMessage>().LongAlert("Not allow 0 quantity!");
            //    UserDialogs.Instance.ShowError("Not allow 0 quantity!", 3000);
            //    QuantityEntry.Focus();
            //    return;
            //}

            if (!string.IsNullOrEmpty(UnitPriceEntry.Text))
            {
                //if (decimal.Parse(UnitPriceEntry.Text) == 0)
                //{
                //    //DependencyService.Get<IMessage>().LongAlert("Not allow 0 Unit Price!");
                //    UserDialogs.Instance.ShowError("Not allow 0 Unit Price!", 3000);
                //    UnitPriceEntry.Focus();
                //    return;
                //}

                UnitPriceEntry.Text = string.Format("{0:0.00}", decimal.Parse(UnitPriceEntry.Text));
                ItemtotalLabel.Text = string.Format("{0:0.00}", decimal.Parse(UnitPriceEntry.Text) * decimal.Parse(QuantityEntry.Text));
                saveButton.Focus();
            }
            else
            {
                //DependencyService.Get<IMessage>().LongAlert("Empty Unit Price!");
                UserDialogs.Instance.ShowError("Empty Unit Price!", 3000);
                this.Focus();
            }
        }

        private void UnitPriceEntry_Completed(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(QuantityEntry.Text))
            {
                QuantityEntry.Focus();
                //DependencyService.Get<IMessage>().LongAlert("Not allow blank  quantity!");
                UserDialogs.Instance.ShowError("Not allow blank  quantity!", 3000);
                return;
            }
            //if (decimal.Parse(QuantityEntry.Text) == 0)
            //{
            //    //DependencyService.Get<IMessage>().LongAlert("Not allow 0 quantity!");
            //    UserDialogs.Instance.ShowError("Not allow 0 quantity!", 3000);
            //    QuantityEntry.Focus();
            //    return;
            //}

            if (!string.IsNullOrEmpty(UnitPriceEntry.Text))
            {
                //if (decimal.Parse(UnitPriceEntry.Text) == 0)
                //{
                //   //DependencyService.Get<IMessage>().LongAlert("Not allow 0 Unit Price!");
                //    UserDialogs.Instance.ShowError("Not allow 0 Unit Price!", 3000);
                //    UnitPriceEntry.Focus();
                //    return;
                //}

                UnitPriceEntry.Text = string.Format("{0:0.00}", decimal.Parse(UnitPriceEntry.Text));
                ItemtotalLabel.Text = string.Format("{0:0.00}", decimal.Parse(UnitPriceEntry.Text) * decimal.Parse(QuantityEntry.Text));
                saveButton.Focus();
            }
            else
            {
                //DependencyService.Get<IMessage>().LongAlert("Empty Unit Price!");
                UserDialogs.Instance.ShowError("Unit Price is empty!", 3000);
                this.Focus();
            }
        }

        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {
            var item = (Button)sender;
            string retval = string.Empty;
            var answer = await UserDialogs.Instance.ConfirmAsync("Are you sure to delete?", "Delete", "Yes", "No");
           // var answer = await DisplayAlert("Delete Sales Order", "Are you sure to delete?", "Yes", "No");
            if (answer)
            {
                DataManager manager = new DataManager();
                manager.DeleteSingleSalesLine(DocNo,ItemNoEntry.Text);

                SalesHeader head = new SalesHeader();
                head = await manager.GetSalesHeaderbyDocNo(DocNo);
                decimal new_amt = (head.TotalAmount - oldLineAmt);
                retval = manager.UpdateSalesHeaderTotalAmount(new_amt, DocNo);
                //DependencyService.Get<IMessage>().LongAlert(retval);
                UserDialogs.Instance.ShowSuccess(retval, 3000);
                Navigation.PopAsync();
            }
        }

        private void FOCQtyEntry_Completed(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(FOCQtyEntry.Text))
            //{
            //    LoadDefaultPrice(ItemNoEntry.Text, App.gCustCode);
            //}
            //else
            //{
            //    DependencyService.Get<IMessage>().LongAlert("Not allow blank foc quantity!");
            //    FOCQtyEntry.Focus();
            //}
        }

        private void QuantityEntry_Completed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(QuantityEntry.Text))
            {
                //if (decimal.Parse(QuantityEntry.Text) == 0)
                //{
                //    //DependencyService.Get<IMessage>().LongAlert("Not allow 0 quantity!");
                //    UserDialogs.Instance.ShowError("Not allow 0 quantity!", 3000);
                //    QuantityEntry.Focus();
                //    return;
                //}
                // LoadDefaultPrice(ItemNoEntry.Text, App.gCustCode);
                ItemtotalLabel.Text = string.Format("{0:0.00}", decimal.Parse(UnitPriceEntry.Text) * decimal.Parse(QuantityEntry.Text));
                UnitPriceEntry.Focus();
            }
            else
            {
                
                //DependencyService.Get<IMessage>().LongAlert("Not allow blank  quantity!");
                UserDialogs.Instance.ShowError("Not allow blank  quantity!", 3000);
                QuantityEntry.Focus();
            }
        }

        private void UnitPricelookUpButton_OnTouchesEnded(object sender, IEnumerable<NGraphics.Point> e)
        {
            var obj = (ActionButton)sender;
            cmdPara = int.Parse(obj.CommandParameter.ToString());
            var page = new PriceLookupPage(ItemNoEntry.Text,App.gCustCode);
            page.pricelistview.ItemSelected += UnitPriceListview_ItemSelected;
            Navigation.PushAsync(page);
        }

      
        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            data = new SalesLine();
            if (!IsBack)
            {
                UserDialogs.Instance.ShowLoading("Loading", MaskType.Black); //IsLoading = true;
                Task.Run(async () =>
                {
                    try
                    {
                        if (EntryNo != 0)
                        {
                            DataManager manager = new DataManager();
                            data = await manager.GetSalesLinebyID(EntryNo);
                        }
                        else
                            data = null;

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayData();
                            UserDialogs.Instance.HideLoading(); //IsLoading = false;
                        });

                    }
                    catch (OperationCanceledException ex)
                    {
                        UserDialogs.Instance.HideLoading(); //IsLoading = false;
                        //DependencyService.Get<IMessage>().LongAlert(ex.Message.ToString());
                        UserDialogs.Instance.ShowError(ex.Message.ToString(), 3000);
                    }
                    catch (Exception ex)
                    {
                        UserDialogs.Instance.HideLoading(); //IsLoading = false;
                        //DependencyService.Get<IMessage>().LongAlert(ex.Message.ToString());
                        UserDialogs.Instance.ShowError(ex.Message.ToString(), 3000);
                    }
                });
            }
        }

        void DisplayData()
        {
            if (EntryNo != 0)
            {
                if (data!=null)
                {
                    //DocumentNoEntry.IsVisible = true;
                    //DocumentNoLabel.IsVisible = true;
                    EntryNo = data.ID;
                    //DocumentNoEntry.Text = dt.Rows[0]["DocumentNo"].ToString();
                    ItemNoEntry.Text = data.ItemNo;
                    DescEntry.Text = data.Description;
                    UomEntry.Text = data.UnitofMeasurementCode;
                    QuantityEntry.Text = data.Quantity.ToString();
                  //  FOCQtyEntry.Text = data.FOCQty.ToString();
                    UnitPriceEntry.Text= string.Format("{0:0.00}", data.UnitPrice);
                    oldLineAmt = data.LineAmount;
                    ItemtotalLabel.Text = string.Format("{0:0.00}", data.LineAmount);
                    isItemExisted = true;

                }
            }
            else
            {
                //DocumentNoLabel.IsVisible = false;
                //DocumentNoEntry.IsVisible = false;
                //DocumentNoEntry.Text = DocNo;
                ItemNoEntry.Text = string.Empty;
                DescEntry.Text = string.Empty;
                UomEntry.Text = string.Empty;
                QuantityEntry.Text = "0";
                //  FOCQtyEntry.Text = "0";
                UnitPriceEntry.Text = string.Format("{0:0.00}", 0);
                // UnitPriceEntry.Text = "0";
                ItemtotalLabel.Text = string.Format("{0:0.00}", 0);
                oldLineAmt = 0;
                isItemExisted = false;
            }
        }

        private void UomlookUpButton_OnTouchesEnded(object sender, IEnumerable<NGraphics.Point> e)
        {
            var obj = (ActionButton)sender;
            string itemNo = ItemNoEntry.Text;
            var page = new LookupUOMPage(itemNo);
            page.listview.ItemSelected += Listview_ItemSelected;
            Navigation.PushAsync(page);
        }

        private void ItemlookUpButton_OnTouchesEnded(object sender, IEnumerable<NGraphics.Point> e)
        {
            var obj = (ActionButton)sender;
            cmdPara = int.Parse(obj.CommandParameter.ToString());
            var page = new ItemsPage();
            page.listview.ItemSelected += ItemListview_ItemSelected;
            Navigation.PushAsync(page);
        }

        private void ItemListview_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;
            IsBack = true;
            var selectedItem = e.SelectedItem as Item;
            ItemNoEntry.Text = selectedItem.ItemNo;
            DescEntry.Text = selectedItem.Description;
            UomEntry.Text = selectedItem.BaseUOM;
            LoadDefaultPrice(selectedItem.ItemNo, App.gCustCode);
            isItemExisted = true;
            //UnitPricePicker.IsEnabled = true;
            Navigation.PopAsync();
        }

        private void UnitPriceListview_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;
            IsBack = true;
            var selectedItem = e.SelectedItem as SalesPrice;
            decimal qty = decimal.Parse(QuantityEntry.Text);
            //  decimal foc = decimal.Parse(FOCQtyEntry.Text);
            UnitPriceEntry.Text = selectedItem.UnitPrice.ToString(); //CalculateSalesPrice(selectedItem.UnitPrice).ToString();
            ItemtotalLabel.Text = string.Format("{0:0.00}", selectedItem.UnitPrice * qty);
          //  LoadSalesPricePicker(selectedItem.ItemNo, App.gCustCode);
            //UnitPricePicker.IsEnabled = true;
            Navigation.PopAsync();
        }

        private decimal CalculateSalesPrice(decimal unitprice)
        {
            decimal qty = decimal.Parse(QuantityEntry.Text);
            decimal foc = 0;// decimal.Parse(FOCQtyEntry.Text);
            decimal saleprice = 0;
            if (qty != 0)
                saleprice = (unitprice * (qty + foc)) / qty;
            else
                saleprice = unitprice;
            return saleprice;
        }

        private  void LoadDefaultPrice(string itemNo, string custcode)
        {
            DataManager dm = new DataManager();
            List<SalesPrice> salesPrices = new List<SalesPrice>();
            salesPrices =  dm.GetItemPricebyItemNo(itemNo, custcode);
            SalesPrice price = salesPrices.OrderBy(x => x.UnitPrice).FirstOrDefault();
            if (price != null)
            {
                UnitPriceEntry.Text = price.UnitPrice.ToString(); //CalculateSalesPrice(price.UnitPrice).ToString();
                if(!string.IsNullOrEmpty(QuantityEntry.Text))
                {
                    decimal qty = decimal.Parse(QuantityEntry.Text);
                    ItemtotalLabel.Text = string.Format("{0:0.00}", decimal.Parse(price.UnitPrice.ToString()) * qty);
                }
                
            }
        }

        private void Listview_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;
            IsBack = true;
            var selectedItem = e.SelectedItem as ItemUOM;
            UomEntry.Text = selectedItem.UOMCode;
            Navigation.PopAsync();
        }

        private async void SaveButton_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ItemNoEntry.Text))
            {
                //DependencyService.Get<IMessage>().LongAlert("Not allow blank Item!");
                UserDialogs.Instance.ShowError("Not allow blank Item!", 3000);
                ItemNoEntry.Focus();
                return;
            }

            if(!isItemExisted)
            {
                UserDialogs.Instance.ShowError("Wrong Item No or Item does not existed!", 3000);
                ItemNoEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(QuantityEntry.Text))
            {
                //DependencyService.Get<IMessage>().LongAlert("Not allow blank quantity!");
                UserDialogs.Instance.ShowError("Not allow blank quantity!", 3000);
                QuantityEntry.Focus();
                return;
            }

            if (decimal.Parse(QuantityEntry.Text) == 0)
            {
                //DependencyService.Get<IMessage>().LongAlert("Not allow 0 quantity!");
                UserDialogs.Instance.ShowError("Not allow 0 quantity!", 3000);
                QuantityEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(UnitPriceEntry.Text))
            {
                //DependencyService.Get<IMessage>().LongAlert("Not allow blank quantity!");
                UserDialogs.Instance.ShowError("Not allow blank unit price!", 3000);
                UnitPriceEntry.Focus();
                return;
            }

            if (decimal.Parse(UnitPriceEntry.Text) == 0)
            {
                //DependencyService.Get<IMessage>().LongAlert("Not allow 0 quantity!");
                UserDialogs.Instance.ShowError("Not allow unit price is 0!", 3000);
                UnitPriceEntry.Focus();
                return;
            }


            DataManager manager = new DataManager();
            SalesLine line= new SalesLine()
            {
                ID = EntryNo,
                DocumentNo = DocNo,
                ItemNo = ItemNoEntry.Text,
                Description = DescEntry.Text,
                UnitofMeasurementCode = UomEntry.Text,
                Quantity = decimal.Parse(QuantityEntry.Text),
                FOCQty = 0,
                UnitPrice = decimal.Parse(UnitPriceEntry.Text)
            };
            string retval = await manager.SaveSQLite_SalesLine(line);
                if (retval == "Success")
                {
                    SalesHeader head = new SalesHeader();
                    head = await manager.GetSalesHeaderbyDocNo(DocNo);
                    decimal new_amt = (head.TotalAmount-oldLineAmt)+ line.LineAmount;
                    retval =  manager.UpdateSalesHeaderTotalAmount(new_amt, DocNo);
                    //DependencyService.Get<IMessage>().LongAlert(retval);
                    UserDialogs.Instance.ShowSuccess(retval, 3000);
                Navigation.PopAsync();
                }
                else
                {
                    //DependencyService.Get<IMessage>().LongAlert(retval);
                UserDialogs.Instance.ShowError(retval, 3000);
            }
            }
    }
}