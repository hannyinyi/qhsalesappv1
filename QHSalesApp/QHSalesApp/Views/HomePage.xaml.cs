﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QHSalesApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        readonly Database database;
        public HomePage()
        {
            InitializeComponent();
            this.Title = "QH Mobile Sales";
            //this.ToolbarItems.Add(new ToolbarItem { Text = "Logout",Icon="exit.png", Command = new Command(this.OnLogout) });
            //Code + EntryNo + Sales Person code + Series
            string codePart = App.gUserEntryNo + App.gSalesPersonCode;
            App.gSOPrefix = "MSO" + codePart;
            App.gCRPrefix = "MCR" + codePart;
            App.gCPPrefix = "MCP" + codePart;

            database = new Database(Constants.DatabaseName);
        }

        private void OnLogout()
        {
            Task.Run(async () =>
            {
                var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
                {
                    Title = "logout",
                    Message = "Are you sure to logout?",
                    CancelText = "No",
                    OkText = "Yes"
                });
                //  var answer = await DisplayAlert("Logout", "Are you sure to logout?", "Yes", "No");
                if (result)
                {
                    Application.Current.MainPage = new NavigationPage(new LoginPage());
                }
            });  
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();

            
            DataManager manager = new DataManager();
            manager.CreateTables();

            // Number Series
            database.CreateTable<NumberSeries>();
            List<NumberSeries> numList = new List<NumberSeries>();
            numList = await manager.GetSQLite_NumberSeries();
            if (numList!=null)
            {
                if(numList.Count==0)
                {
                    ObservableCollection<NumberSeries> numSeries = new ObservableCollection<NumberSeries>();
                    numSeries.Add(new NumberSeries() { Code = App.gSOPrefix, Description = "Sales Order", Increment = 1, LastNoCode = App.gSOPrefix + "-10000", LastNoSeries = 10000 });
                    numSeries.Add(new NumberSeries() { Code = App.gCRPrefix, Description = "Credit Memo", Increment = 1, LastNoCode = App.gCRPrefix + "-10000", LastNoSeries = 10000 });
                    numSeries.Add(new NumberSeries() { Code = App.gCPPrefix, Description = "Customer Payment", Increment = 1, LastNoCode = App.gCPPrefix + "-10000", LastNoSeries = 10000 });
                    manager.SaveSQLite_NumberSeries(numSeries);
                }               
            } 
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (await DisplayAlert("Exit page?", "Are you sure you want to exit this page? You will not be able to continue it.", "Yes", "No"))
                {
                    base.OnBackButtonPressed();

                    Application.Current.MainPage = new NavigationPage(new LoginPage());
                }
            });

            // Always return true because this method is not asynchronous.
            // We must handle the action ourselves: see above.
            return true;
        }
    }
}