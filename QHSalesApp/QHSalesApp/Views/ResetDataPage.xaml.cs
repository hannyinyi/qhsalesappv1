﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QHSalesApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResetDataPage : ContentPage
    {
        public ResetDataPage()
        {
            InitializeComponent();
            this.Title = "Reset Data";
            NavigationPage.SetHasBackButton(this, false);
            this.BackgroundColor = Color.FromHex("#dddddd");
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            Application.Current.MainPage = new NavigationPage(new MainPage(0));

            // Always return true because this method is not asynchronous.
            // We must handle the action ourselves: see above.
            return true;
        }

        private void ResetTransButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                DataManager manager = new DataManager();
                manager.ResetTransData();
                ObservableCollection<NumberSeries> numSeries = new ObservableCollection<NumberSeries>();
                numSeries.Add(new NumberSeries() { Code = App.gSOPrefix, Description = "Sales Order", Increment = 1, LastNoCode = App.gSOPrefix + "-10000", LastNoSeries = 10000 });
                numSeries.Add(new NumberSeries() { Code = App.gCRPrefix, Description = "Credit Memo", Increment = 1, LastNoCode = App.gCRPrefix + "-10000", LastNoSeries = 10000 });
                numSeries.Add(new NumberSeries() { Code = App.gCPPrefix, Description = "Customer Payment", Increment = 1, LastNoCode = App.gCPPrefix + "-10000", LastNoSeries = 10000 });
                manager.SaveSQLite_NumberSeries(numSeries);
                UserDialogs.Instance.ShowSuccess("Reset Transaction Data Successful!", 3000);

            }
            catch (Exception ex)
            {

                DependencyService.Get<IMessage>().ShortAlert(ex.Message.ToString());
            }
        }

        private void ResetMasterButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                DataManager manager = new DataManager();
                manager.resetMasterData();
               // manager.SaveSQLite_NumberSeries();
               // DependencyService.Get<IMessage>().ShortAlert("Reset Successful !");
                UserDialogs.Instance.ShowSuccess("Reset Master Data Successful!", 3000);

            }
            catch (Exception ex)
            {

                DependencyService.Get<IMessage>().ShortAlert(ex.Message.ToString());
            }
        }
    }
}