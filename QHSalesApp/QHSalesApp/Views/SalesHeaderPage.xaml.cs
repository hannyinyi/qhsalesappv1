﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QHSalesApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SalesHeaderPage : ContentPage
    {
        
        private ObservableCollection<SalesHeader> recItems { get; set; }
        private bool _isloading;

        public bool IsLoading
        {
            get { return this._isloading; }
            set
            {
                this._isloading = value;
                OnPropertyChanged("IsLoading");
            }
        }
        public SalesHeaderPage()
        {
            InitializeComponent();

            if (App.gDocType == "SO")
                this.Title = "Sales Order (Open)";
            else
                this.Title = "Credit Memo (Open)";
           
            NavigationPage.SetHasBackButton(this, false);
            this.BackgroundColor = Color.FromHex("#dddddd");
            //this.ToolbarItems.Add(new ToolbarItem { Text = "Open", Command = new Command(this.LoadOpenSO) });
            this.ToolbarItems.Add(new ToolbarItem { Text = "Released", Command = new Command(this.LoadReleasedSO) });
            listview.ItemTapped += Listview_ItemTapped;
            sbSearch.Placeholder = "Search by Order No,Posting Date";
            sbSearch.TextChanged += (sender2, e2) => FilterKeyword(sbSearch.Text);
            sbSearch.SearchButtonPressed += (sender2, e2) => FilterKeyword(sbSearch.Text);
            
           // EmptyLayout.IsVisible = false;
            IsLoading = false;
            BindingContext = this;
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            Application.Current.MainPage = new NavigationPage(new MainPage(0));

            // Always return true because this method is not asynchronous.
            // We must handle the action ourselves: see above.
            return true;
        }
        //async void LoadOpenSO()
        //{
        //    App.gSOStatus = "Open";
        //    this.Title = "Open Orders";
        //    await LoadData(App.gSOStatus);
        //}

        void LoadReleasedSO()
        {
           App.gSOStatus = "Released";
           //this.Title = "Released Sales Orders";
            Navigation.PushAsync(new MainPage(2));
        }

        private void Listview_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null) return;
            ((ListView)sender).SelectedItem = null; // de-select the row
            var item = (SalesHeader)e.Item;
            if (App.gDocType == "SO")
                App.gPageTitle = "Edit Sales Order (" + item.DocumentNo+")";
            else
                App.gPageTitle = "Edit Credit Memo (" + item.DocumentNo + ")";
            Navigation.PushAsync(new SalesOrderEntryPage(item.ID));
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing(); 
           await LoadData(App.gSOStatus);
       
            //if(listview.ItemsSource==null)
            //{
            //    DataLayout.IsVisible = false;
            //   EmptyLayout.IsVisible = true;
            //}
            //else
            //{
            //    DataLayout.IsVisible = true;
            //    EmptyLayout.IsVisible = false;
            //}
        }

        //async Task BindRecords(string docNo)
        //{
        //    DataTable dt = new DataTable();
        //    dt = await App.svcManager.RetrieveTfHeaders(docNo, App.DocuType);

        //    if (dt.Rows.Count > 0)
        //    {
        //        recItems = new ObservableCollection<TransferHeader>();
        //        //int i = 0;
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            recItems.Add(new TransferHeader
        //            {
        //                ID = int.Parse(dr["EntryNo"].ToString()),
        //                DocumentNo = (dr["DocumentNo"].ToString()),
        //                DocumentType = dr["DocumentType"].ToString(),
        //                DocumentDate = dr["DocumentDate"].ToString(),
        //                FromLoc = dr["FromLoc"].ToString(),
        //                ToLoc = dr["ToLoc"].ToString(),
        //                Status = dr["Status"].ToString(),
        //            });
        //        }

        //    }
        //}

        async Task LoadData(string status)
        {
            UserDialogs.Instance.ShowLoading("Loading", MaskType.Black); //IsLoading = true;
            Task.Run(async () =>
            {
                try
                {
                    recItems = new ObservableCollection<SalesHeader>();
                    DataManager manager = new DataManager();
                    recItems = await manager.GetSQLite_SalesHeaderbyStatus(status,App.gDocType);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (recItems != null)
                        {
                            listview.ItemsSource = recItems.OrderByDescending(x => x.ID);
                        }
                        else
                        {
                            listview.ItemsSource = null;
                           
                        }
                        listview.Unfocus();
                        UserDialogs.Instance.HideLoading(); //IsLoading = false;
                    });
                   
                }
                catch (OperationCanceledException ex)
                {
                    UserDialogs.Instance.HideLoading(); //IsLoading = false;
                    //DependencyService.Get<IMessage>().LongAlert(ex.Message.ToString());
                    UserDialogs.Instance.ShowError(ex.Message.ToString(), 3000);
                }
                catch (Exception ex1)
                {
                    UserDialogs.Instance.HideLoading(); //IsLoading = false;
                    //DependencyService.Get<IMessage>().LongAlert(ex.Message.ToString());
                    UserDialogs.Instance.ShowError(ex1.Message.ToString(), 3000);
                }
            });
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
        }

        private void FilterKeyword(string filter)
        {
            if (recItems == null) return;
            listview.BeginRefresh();
            if (string.IsNullOrWhiteSpace(filter))
            {
                listview.ItemsSource = recItems.OrderByDescending(x => x.ID);

            }
            else
            {
                listview.ItemsSource = recItems.Where(x => x.DocumentNo.ToLower().Contains(filter.ToLower()) ||
                x.DocumentDate.ToString().ToLower().Contains(filter.ToLower()));
            }
            listview.EndRefresh();
        }

        private void DtlButton_OnTouchesEnded(object sender, IEnumerable<NGraphics.Point> e)
        {
            var item = (ActionButton)sender;
            if (App.gDocType == "SO")
                App.gPageTitle = "Items List (Sales Order)";
            else
                App.gPageTitle = "Items List (Credit Memo)";
            SalesHeader head = new SalesHeader();
            head = recItems.Where(x => x.DocumentNo == item.CommandParameter.ToString()).FirstOrDefault();
            App.gCustCode = head.SellToCustomer;
            Navigation.PushAsync(new SalesLinePage(item.CommandParameter.ToString(),"Open"));
        }

        private void AddButton_OnTouchesEnded(object sender, IEnumerable<NGraphics.Point> e)
        {
            if(App.gDocType=="SO")
                App.gPageTitle = "Add New Sales Order";
            else
                App.gPageTitle = "Add New Credit Memo";
            Navigation.PushAsync(new SalesOrderEntryPage(0));
        }

        private async void DeleteButton_OnTouchesEnded(object sender, IEnumerable<NGraphics.Point> e)
        {
            var item = (ActionButton)sender;
            var answer = await DisplayAlert("Confirmation", "Are you sure to delete?", "Yes", "No");
            if (answer)
            {
                DataManager manager = new DataManager();
                manager.DeleteSalesOrderbyID(int.Parse(item.CommandParameter.ToString()));
                SalesHeader hd = new SalesHeader();
                hd =await manager.GetSalesHeaderbyID(int.Parse(item.CommandParameter.ToString()));
                manager.DeleteSalesLinebyDocNo(hd.DocumentNo);
                await LoadData(App.gSOStatus);
            }
        }

        private void DetailTapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            //var item = (GestureRecognizer)sender;
            //App.gPageTitle = "Sales Items";
            //SalesHeader head = new SalesHeader();
            //head = recItems.Where(x => x.DocumentNo == item.CommandParameter.ToString()).FirstOrDefault();
            //App.gCustCode = head.SellToCustomer;
            //Navigation.PushAsync(new SalesLinePage(head.DocumentNo));
        }

        private async Task DeleteTapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            //var answer = await DisplayAlert("Delete Sales Order", "Are you sure to delete?", "Yes", "No");
            //if (answer)
            //{
            //    DataManager manager = new DataManager();
            //    manager.DeleteSalesOrderbyID(id);
            //}
        }

        private void DetailButton_Clicked(object sender, EventArgs e)
        {
            var item = (Button)sender;
            if (App.gDocType == "SO")
                App.gPageTitle = "Items List (Sales Order)";
            else
                App.gPageTitle = "Items List (Credit Memo)";
            SalesHeader head = new SalesHeader();
            head = recItems.Where(x => x.DocumentNo == item.CommandParameter.ToString()).FirstOrDefault();
            App.gCustCode = head.SellToCustomer;
            Navigation.PushAsync(new SalesLinePage(item.CommandParameter.ToString(),"Open"));
        }

        private async Task DeleteButton_Clicked(object sender, EventArgs e)
        {
            var item = (Button)sender;
            
            var answer = await DisplayAlert("Confirmation", "Are you sure to delete?", "Yes", "No");
            if (answer)
            {
                DataManager manager = new DataManager();
                manager.DeleteSalesOrderbyID(int.Parse(item.CommandParameter.ToString()));
                await LoadData(App.gSOStatus);
            }
        }
    }
}