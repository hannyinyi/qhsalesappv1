﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QHSalesApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SetupPage : ContentPage
    {
        private string deviceIdentifier { get; set; }
        readonly Database database;
        SvcConnection svc;

        public SetupPage()
        {
            InitializeComponent();
            var navipage = Application.Current.MainPage as NavigationPage;
            navipage.BarBackgroundColor = Color.Black;
            database = new Database(Constants.DatabaseName);
            database.CreateTable<User>();
           
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadSetting();
            
            NavigationPage.SetHasBackButton(this, true);
            IDevice device = DependencyService.Get<IDevice>();
            deviceIdentifier = device.GetIdentifier();
            Title = "Device Registration";
            this.BackgroundColor = Color.FromHex("#dddddd");
            RegisterButton.Clicked += RegisterButton_Clicked;
         
        }


        async void RegisterButton_Clicked(object sender, EventArgs e)
        {
            string retval = string.Empty;
            if (!string.IsNullOrEmpty(WebServiceUrlEntry.Text))
            {
                if (!IsValidURL(WebServiceUrlEntry.Text.Trim()))
                {
                    //DependencyService.Get<IMessage>().ShortAlert("Error : Web service URL is invalid!");
                    UserDialogs.Instance.ShowError("Error : Web service URL is invalid!", 3000);
                    return;
                }

                if (string.IsNullOrEmpty(MobileAccessKeyEntry.Text))
                {
                    //DependencyService.Get<IMessage>().ShortAlert("Required Mobile Access Key !");
                    UserDialogs.Instance.ShowError("Required Mobile Access Key !", 3000);
                    return;
                }

                var answer = DisplayAlert("Confirm", "Are you sure to register device?", "Yes", "No");
                if (await answer)
                {
                    DependencyService.Get<INetworkConnection>().CheckNetworkConnection();
                    if (DependencyService.Get<INetworkConnection>().IsConnected)
                    {
                        string result = DependencyService.Get<INetworkConnection>().IsServiceOnline(WebServiceUrlEntry.Text.Trim());
                        if (result != "true")
                        {
                            //DependencyService.Get<IMessage>().LongAlert("Error : Service is offline. [" + result + "]");
                            UserDialogs.Instance.ShowError("Error : Service is offline. [" + result + "]", 3000);
                            return;
                        }


                        // *** Save WebService Url and Mobile Access Key to Local ***
                        if (svc == null) svc = new SvcConnection() { svcUrl = WebServiceUrlEntry.Text.Trim(), deviceAccessKey = MobileAccessKeyEntry.Text.Trim() };
                        else
                        {
                            svc.svcUrl = WebServiceUrlEntry.Text.Trim();
                            svc.deviceAccessKey = MobileAccessKeyEntry.Text.Trim();
                        }

                        Helpers.Settings.GeneralSettings = svc.svcUrl;
                        Helpers.Settings.DeviceAccessKey = svc.deviceAccessKey;

                        retval = App.svcManager.DeviceRegister(MobileAccessKeyEntry.Text, deviceIdentifier);
                        if (retval == "Success")
                        {
                            DataManager manager = new DataManager();
                            manager.SaveSQLite_Users(MobileAccessKeyEntry.Text);
                            //DependencyService.Get<IMessage>().ShortAlert("Device registered success!");
                            UserDialogs.Instance.ShowSuccess("Device registered success!", 3000);
                        }  
                        else
                            UserDialogs.Instance.ShowError("Error : " + retval, 3000);
                        //DependencyService.Get<IMessage>().ShortAlert("Error : " + retval);


                    }
                    else
                        UserDialogs.Instance.ShowError("Error : No internet connection", 3000);
                    //DependencyService.Get<IMessage>().ShortAlert("Error : No internet connection");
                }
                { }
            }
            else
                UserDialogs.Instance.ShowError("Required service Url!", 3000);
            //DependencyService.Get<IMessage>().ShortAlert("Error : Url cannot be empty");


        }

        private void LoadSetting()
        {
            string setting_url = string.Empty;
            string device_access_key = string.Empty;
            try
            {
                setting_url = Helpers.Settings.GeneralSettings;
                device_access_key = Helpers.Settings.DeviceAccessKey;
            }
            catch (System.NullReferenceException) { }
            catch (System.Collections.Generic.KeyNotFoundException) { }

            if (string.IsNullOrEmpty(setting_url))
            {
                //DependencyService.Get<IMessage>().ShortAlert("NAV URL not setup yet");
               // UserDialogs.Instance.ShowError("NAV URL not setup yet", 3000);
                WebServiceUrlEntry.Text = Constants.SoapUrl;
                MobileAccessKeyEntry.Text = string.Empty;
            }
            else
            {
                svc = new SvcConnection() { svcUrl = setting_url, deviceAccessKey = device_access_key };
                if (svc != null)
                {
                    WebServiceUrlEntry.Text = svc.svcUrl;
                    MobileAccessKeyEntry.Text = svc.deviceAccessKey;
                }
            }
        }

        private bool IsValidURL(string url)
        {
            Uri uri = null;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uri) || null == uri)
                return false;
            return true;
        }
    }
}