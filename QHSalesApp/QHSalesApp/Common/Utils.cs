﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QHSalesApp
{
    public static class Utils
    {
       public static string Print_Payment(string SelectedBthDevice,Payment record)
        {
            string retval = string.Empty;
            try
            {
                string NewLine = "\x0A";
                string cmds = "\x1b\x61\x01\x1b\x45\x01\x1b\x21\x10\x1b\x21\x20      Payment Receipt \x1b\x45\x00";
                // string cmds ="\x1b\x45\x01" + "Payment Receipt";// = "\x1B\x45\x01";
                cmds += NewLine;
                cmds += "\x1b\x61\x01\x1b\x45\x01\x1b\x21\x10\x1b\x21\x20    Qian Hu Fish Farm Trading \x1b\x45\x00"; //text to print
                cmds += NewLine;
                cmds += "\x1b\x61\x01     No.71 Jalan Lekar Singapore 698950";
                cmds += NewLine;
                cmds += "\x1b\x21\x00  Date: " + record.OnDate + "                    SalesPerson : " + record.SalesPersonCode;
                cmds += NewLine;
                cmds += "\x1b\x21\x00  Cust No: " + record.CustomerNo;
                cmds += NewLine;
                cmds += "\x1b\x21\x00  Cust Name: " + record.CustomerName;
                cmds += NewLine;
                cmds += "\x1b\x21\x00  Receipt No. : " + record.DocumentNo;
                cmds += NewLine;
                cmds += "\x1b\x21\x00  Ref Document No : " + record.RefDocumentNo;
                cmds += NewLine;
                cmds += "\x1b\x21\x00  -------------------------------------------------------";
                cmds += NewLine;
                cmds += "\x1b\x45\x01\x1b\x21\x10\x1b\x21\x20    Amount(SGD):   $" + record.Amount + " \x1b\x45\x00";
                cmds += NewLine;
                cmds += NewLine;
                //ESC = leftAlign + "\x1B\x46\x00";
                cmds += "\x1b\x21\x00  Remarks : " + record.Note;
                cmds += NewLine;
                cmds += NewLine;
                cmds += "\x1b\x21\x00  -------------------------------------------------------";
                cmds += NewLine;
                cmds += "\x1b\x21\x00  -------------------------------------------------------";
                //cmds += "\x1b\x61\x02\x1b\x21\x00                           Customer Signature"; // Signature should be shown as right side.
                cmds += NewLine;

                retval = DependencyService.Get<IBluetoothPrinter>().Print(SelectedBthDevice, cmds);
                return retval;
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }
    }
}
