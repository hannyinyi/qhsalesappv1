﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QHSalesApp
{
    public class CustLedgerEntry : BaseItem
    {
        public string TransType { get; set; }
        public string DocNo { get; set; }
        public string CustNo { get; set; }
        public string ExtDocNo { get; set; }
        public string IsOpenItem { get; set; }
        public string TransDate { get; set; }
        public string PaymentTerm { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal UnpaidAmount { get; set; }
    }
}
