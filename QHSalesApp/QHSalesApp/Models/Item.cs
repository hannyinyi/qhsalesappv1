﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QHSalesApp
{
    public class Item : BaseItem
    {
        public int EntryNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string BaseUOM { get; set; }
        public string UnitPrice { get; set; }
        public string CategoryCode { get; set; }
        public decimal InvQty { get; set; }
    }
}
