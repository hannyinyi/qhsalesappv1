﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QHSalesApp
{
    public  class ServiceManager
    {
        ISoapService soapService;

        public ServiceManager(ISoapService svc)
        {
            soapService = svc;
        }

        public string DeviceRegister(string accesskey, string deviceId)
        {
            return soapService.DeviceRegistration(accesskey, deviceId);
        }

        public string CheckUserLogin(string mobileAccessKey,string deviceId, string email, string password)
        {
            return soapService.UserLogin(mobileAccessKey,deviceId ,email, password);
        }

        //public async Task<DataTable> RetLoginData(string usrEmail, string usrPassword)
        //{
        //    DataTable dt = new DataTable();
        //    dt = soapService.get
        //    return dt;
        //}

        public DataTable RetLoginUsers(string mobileAccessKey)
        {
            return soapService.GetLoginUsers(mobileAccessKey);
        }

        public DataTable RetItems()
        {
            return soapService.GetItems();
        }

        public DataTable RetItemUOMs()
        {
            return soapService.GetItemUOMs();
        }

        public DataTable RetItemSalesPrices(string salesPersonCode)
        {
            return soapService.GetItemSalesPrices(salesPersonCode);
        }

        public DataTable RetCustomers(string salesPersonCode)
        {
            return soapService.GetCustomers(salesPersonCode);
        }

        public async Task <DataTable> RetCustomerPriceHistory(string custno,string itemno)
        {
            return soapService.GetCustomerPriceHistory(custno,itemno);
        }

        public DataTable RetCustomerLedgerEntry(string custno)
        {
            return soapService.GetCustLedgerEntry(custno);
        }

        public DataTable RetPaymentMethods()
        {
            return soapService.GetPaymentMethods();
        }

        public string ExportSalesHeader(string docNo, string selltocustomer, string selltoName, string billtoCustomer, string billtoName, string docDate, string status, string paymentMethod, decimal totalAmt,string doctype,string note, string strSingature)
        {
            return soapService.ExportSalesHeader(docNo,selltocustomer,selltoName,billtoCustomer,billtoName,docDate,status,paymentMethod,totalAmt,doctype,note,strSingature);
        }

        public string ExportSalesLine(string docNo, string itemNo, string locCode, decimal qty, decimal focQty, string uom, decimal unitPrice, decimal lineDisPercent, decimal lineDisAmt, decimal lineAmt)
        {
            return soapService.ExportSalesLine(docNo, itemNo, locCode, qty, focQty, uom, unitPrice, lineDisPercent, lineDisAmt, lineAmt);
        }

        public string ExportPayment(string docNo, string onDate, string customerNo, string paymentMethod, decimal amount, string strSignature, string strImage, string salesPersonCode, string note, string recStatus,string refdocno,string sourcetype)
        {
            return soapService.ExportPayment(docNo, onDate, customerNo, paymentMethod, amount, strSignature, strImage, salesPersonCode, note, recStatus,refdocno,sourcetype);
        }

        public DataTable ConvertDataTable<T>(IEnumerable<T> list)
        {
            return soapService.CreateDataTable<T>(list);
        }

      
    }
}
