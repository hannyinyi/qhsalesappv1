﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using Xamarin.Forms;
using System.Data;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;

namespace QHSalesApp
{
    public  class DataManager
    {
        readonly Database database;
        public DataManager()
        {
            database = new Database(Constants.DatabaseName);
            
        }

        public string SaveSQLite_Users(string mobileAccessKey)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = App.svcManager.RetLoginUsers(mobileAccessKey);

                if (dt.Rows.Count > 0)
                {
                    database.DeleteAll<User>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        var record = new User
                        {
                            EntryNo = int.Parse(dr["EntryNo"].ToString()),
                            Default_CustEntryNo = int.Parse(dr["Default_CustEntryNo"].ToString()),
                            Email = dr["Email"].ToString(),
                            UserID = dr["ID"].ToString(),
                            Role = dr["Role"].ToString(),
                            Name = dr["Name"].ToString(),
                            pwd = dr["pwd"].ToString(),
                            Status = dr["Status"].ToString().ToLower() == "true" ? true : false,
                            Outlet_Loc = dr["Default_Loc"].ToString(),
                            WH_Loc = dr["Main_Loc"].ToString(),
                            SalesPersonCode=dr["SalesPersonCode"].ToString()
                        };

                        database.SaveData(record);
                    }
                    return "Success";
                }
                else
                    return "No Users!";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }
        public void DeleteItem()
        {
            database.DeleteAll<Item>();
        }

        public void DeleteSalesOrder()
        {
            database.DeleteAll<SalesHeader>();
        }

        public void DeletePayment()
        {
            database.DeleteAll<Payment>();
        }
        public void DeleteSalesOrderbyID(int id)
        {
            database.DeleteData<SalesHeader>(id);
        }
        public void CreateTables()
        {
            database.CreateTable<SalesHeader>();
            database.CreateTable<SalesLine>();
            database.CreateTable<Payment>();
            database.CreateTable<Item>();
            database.CreateTable<SalesPrice>();
            database.CreateTable<Customer>();
            database.CreateTable<PaymentMethod>();
            database.CreateTable<CustLedgerEntry>();
            database.CreateTable<PaidReference>();

        }

        public void ResetTransData()
        {
            database.DeleteAll<NumberSeries>();
            database.DeleteAll<SalesHeader>();
            database.DeleteAll<SalesLine>();
            database.DeleteAll<Payment>();
            database.DeleteAll<PaidReference>();
            database.DeleteAll<NumberSeries>();
        }

        public void resetMasterData()
        {
            database.DeleteAll<Item>();
            database.DeleteAll<SalesPrice>();
            database.DeleteAll<Customer>();
            database.DeleteAll<PaymentMethod>();
            database.DeleteAll<CustLedgerEntry>();
            //  database.DeleteAll<PaymentHistory>();
            //database.DeleteAll<CustomerPriceHistory>();
        }

        public async Task<string> SaveSQLite_Items()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = App.svcManager.RetItems();

                if (dt.Rows.Count > 0)
                {
                    database.DeleteAll<Item>();
                    //IEnumerable items = dt.Select().AsEnumerable();
                    foreach (DataRow dr in dt.Rows)
                    {
                        var record = new Item
                        {
                            EntryNo = int.Parse(dr["EntryNo"].ToString()),
                            ItemNo = dr["No"].ToString(),
                            Description = dr["Description"].ToString(),
                            Description2 = dr["Description2"].ToString(),
                            BaseUOM = dr["BaseUOM"].ToString(),
                            UnitPrice = dr["Unit_Price"].ToString(),
                            CategoryCode = dr["CategoryCode"].ToString(),
                            InvQty=decimal.Parse(dr["InvQty"].ToString())
                        };
                        database.SaveData<Item>(record);
                    }

                    //database.SaveDataAll(items);
                    return "Success";
                }
                else
                    return "No Items!";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public async Task<string> SaveSQLite_ItemUOms()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = App.svcManager.RetItemUOMs();

                if (dt.Rows.Count > 0)
                {
                    database.DeleteAll<ItemUOM>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        var record = new ItemUOM
                        {
                            EntryNo = int.Parse(dr["EntryNo"].ToString()),
                            ItemNo = dr["ItemNo"].ToString(),
                            UOMCode = dr["UOMCode"].ToString(),
                        };
                        database.SaveData(record);
                    }
                    return "Success Save rows : " + dt.Rows.Count;
                }
                else
                    return "No Item UOMs!";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public async Task<string> SaveSQLite_SalesPrices(string salesPersonCode)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = App.svcManager.RetItemSalesPrices(salesPersonCode);

                if (dt.Rows.Count > 0)
                {
                    database.DeleteAll<SalesPrice>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        var record = new SalesPrice
                        {
                            EntryNo = int.Parse(dr["EntryNo"].ToString()),
                            SalesType = dr["SalesType"].ToString(),
                            ItemNo = dr["ItemNo"].ToString(),
                            SalesCode = dr["SalesCode"].ToString(),
                            MinimumQty = decimal.Parse(dr["MinimumQty"].ToString()),
                            UnitPrice = decimal.Parse(dr["UnitPrice"].ToString()),
                            StartDate = Convert.ToDateTime(dr["StartDate"].ToString()),
                            EndDate = Convert.ToDateTime(dr["EndDate"].ToString()),
                            UOM = dr["UOM"].ToString(),
                            CustomerNo = dr["CustomerNo"].ToString(),
                            PromotionType = dr["PromotionType"].ToString()
                        };
                        database.SaveData< SalesPrice>(record);
                    }
                    return "Success";
                }
                else
                    return "No Sales Prices!";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public async Task<string> SaveSQLite_Customers(string salesPersonCode)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = App.svcManager.RetCustomers(salesPersonCode);

                if (dt.Rows.Count > 0)
                {
                    database.DeleteAll<Customer>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        var record = new Customer
                        {
                            EntryNo = int.Parse(dr["EntryNo"].ToString()),
                            CustomerNo= dr["CustomerNo"].ToString(),
                            Country= dr["Country"].ToString(),
                            SalesPersonCode= dr["SalesPersonCode"].ToString(),
                            Name= dr["Name"].ToString(),
                            Name2= dr["Name2"].ToString(),
                            SearchName= dr["SearchName"].ToString(),
                            Contact= dr["Contact"].ToString(),
                            Address= dr["Address"].ToString(),
                            Address2= dr["Address2"].ToString(),
                            City= dr["City"].ToString(),
                            Postcode= dr["Postcode"].ToString(),
                            CountryCode= dr["CountryCode"].ToString(),
                            PhoneNo= dr["PhoneNo"].ToString(),
                            MobileNo= dr["MobileNo"].ToString(),
                            TelexNo= dr["TelexNo"].ToString(),
                            FaxNo= dr["FaxNo"].ToString(),
                            Email= dr["Email"].ToString(),
                            Website= dr["Website"].ToString(),
                            CreditLimit= dr["CreditLimit"].ToString(),
                            InvoiceLimit= dr["InvoiceLimit"].ToString(),
                            Outstanding= dr["Outstanding"].ToString(),
                            CurrencyCode= dr["CurrencyCode"].ToString(),
                            PaymentTerms= dr["PaymentTerms"].ToString(),
                            CustomerPriceGroup= dr["CustomerPriceGroup"].ToString(),
                            CustomerDiscGroup= dr["CustomerDiscGroup"].ToString()
                        };
                        database.SaveData<Customer>(record);
                    }
                    return "Success";
                }
                else
                    return "No customers!";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public async Task<string> SaveSQLite_CustLederEntry(string custno)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = App.svcManager.RetCustomerLedgerEntry(custno);

                if (dt.Rows.Count > 0)
                {
                    database.DeleteAll<CustLedgerEntry>();
                    { }
                    foreach (DataRow dr in dt.Rows)
                    {
                        var record = new CustLedgerEntry
                        {
                            //  EntryNo = int.Parse(dr["EntryNo"].ToString()),
                            TransType = dr["TransType"].ToString(),
                            DocNo = dr["DocNo"].ToString(),
                            CustNo=dr["CustNo"].ToString(),
                            ExtDocNo=dr["ExtDocNo"].ToString(),
                            IsOpenItem= dr["IsOpenItem"].ToString(),
                            TransDate= dr["TransDate"].ToString(),
                            PaymentTerm= dr["PaymentTerm"].ToString(),
                            InvoiceAmount= decimal.Parse(dr["InvoiceAmount"].ToString()),
                            PaidAmount= decimal.Parse(dr["PaidAmount"].ToString()),
                            UnpaidAmount= decimal.Parse(dr["UnpaidAmount"].ToString())
                        };
                        database.SaveData<CustLedgerEntry>(record);
                    }
                    return "Success";
                }
                else
                    return "No customers Ledger Entry!";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        

        //public async Task<string> SaveSQLite_CustomerPriceHistory(string custno,string itemno)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        dt = App.svcManager.RetCustomerPriceHistory();

        //        if (dt.Rows.Count > 0)
        //        {
        //            database.DeleteAll<CustomerPriceHistory>();
        //            foreach (DataRow dr in dt.Rows)
        //            {
        //                var record = new CustomerPriceHistory
        //                {
        //                   CustNo = dr["CustNo"].ToString(),
        //                    ItemNo = dr["ItemNo"].ToString(),
        //                    UOM = dr["UOM"].ToString(),
        //                    Currency = dr["Currency"].ToString(),
        //                    UnitPrice = dr["UnitPrice"].ToString(),
        //                    Qty = dr["Qty"].ToString(),
        //                    TransDate = dr["TransDate"].ToString(),
        //                    UnitPrice2 = dr["unitPrice2"].ToString(),
        //                    Qty2 = dr["Qty2"].ToString(),
        //                    TransDate2 = dr["TransDate2"].ToString(),
        //                    unitPrice3 = dr["unitPrice3"].ToString(),
        //                    Qty3 = dr["Qty3"].ToString(),
        //                    TransDate3 = dr["TransDate3"].ToString()
        //                };
        //                database.SaveData<CustomerPriceHistory>(record);
        //            }
        //            return "Success";
        //        }
        //        else
        //            return "No Customer Price History!";
        //    }
        //    catch (Exception ex)
        //    {

        //        return ex.Message.ToString();
        //    }
        //}

        public async Task<string> SaveSQLite_PaymentMethods()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = App.svcManager.RetPaymentMethods();

                if (dt.Rows.Count > 0)
                {
                    database.DeleteAll<PaymentMethod>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        var record = new PaymentMethod
                        {
                           
                            Code = dr["Code"].ToString(),
                            Description = dr["Description"].ToString(),
                        };
                        database.SaveData<PaymentMethod>(record);
                    }
                    return "Success";
                }
                else
                    return "No Payment Methods!";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public  User LoadSQLite_User(string email, string pswd)
        {
            string[] parameters = new string[] { email, pswd };
            var user = database.Query<User>("Select * from User WHERE Email=? AND pwd=?", parameters);
            return user.FirstOrDefault();
        }

        public User GetSQLlite_User()
        {
            User user = new User();
            var obj = database.GetData<User>();
            user = obj.FirstOrDefault(); //Where(i => i.CustomerNo.Trim() == custno).
            return user;
        }

        public async Task<List<Item>> GetSQLite_Items()
        {
            List<Item> items = new List<Item>();
            var obj = database.GetData<Item>();
            if (items != null) items = obj.ToList();
            return items;
        }

        public async Task<Item> GetSQLite_ItembyItemNo(string itemno)
        {
            Item item = new Item();
            var obj = database.GetData<Item>();
            item = obj.Where(i => i.ItemNo.Trim() == itemno).FirstOrDefault();
            return item;
        }

        public async Task<List<ItemUOM>> GetSQLite_ItemUOMs()
        {
            List<ItemUOM> items = new List<ItemUOM>();
            var obj = database.GetData<ItemUOM>();
            if (items != null) items = obj.ToList();
            return items;
        }

        public async Task<List<SalesPrice>> GetSQLite_SalesPrices()
        {
            List<SalesPrice> items = new List<SalesPrice>();
            var obj = database.GetData<SalesPrice>();
            if (items != null) items = obj.ToList();
            return items;
        }

        public List<PaymentMethod> GetSQLite_PaymentMethods()
        {
            List<PaymentMethod> items = new List<PaymentMethod>();
            var obj = database.GetData<PaymentMethod>();
            if (items != null) items = obj.ToList();
            return items;
        }

        public async Task<List<Customer>> GetSQLite_Customers()
        {
            List<Customer> items = new List<Customer>();
            var obj = database.GetData<Customer>();
            if (obj != null) items = obj.Where(i => i.Name.Trim() != string.Empty).ToList();
            //items = obj.Where(i => i.ItemNo == itemno).ToList();
            return items;
        }

        public  Customer GetSQLite_CustomerbyCustNo(string custno)
        {
            Customer customer = new Customer();
            var obj = database.GetData<Customer>();
            customer = obj.Where(i => i.CustomerNo.Trim() == custno).FirstOrDefault();
            return customer;
        }

        public async Task<List<CustLedgerEntry>> GetSQLite_CustomerLedgerEntry(string custno)
        {
            List<CustLedgerEntry> items = new List<CustLedgerEntry>();
            var obj = database.GetData<CustLedgerEntry>();
            if (items != null) items = obj.Where(i => i.CustNo.Trim() == custno).ToList();
            //items = obj.Where(i => i.ItemNo == itemno).ToList();
            return items;
        }

        public async Task<List<CustLedgerEntry>> GetSQLite_CLEUnpaidBill(string custno)
        {
            List<CustLedgerEntry> items = new List<CustLedgerEntry>();
            var obj = database.GetData<CustLedgerEntry>();
            if (items != null) items = obj.Where(i => i.TransType.Trim() == "Invoice" && i.UnpaidAmount>0 && i.CustNo==custno).ToList();
            //items = obj.Where(i => i.ItemNo == itemno).ToList();
            return items;
        }

        public async Task<List<SalesHeader>> GetSQLite_SalesHeadersbyCustNo(string custno)
        {
            List<SalesHeader> items = new List<SalesHeader>();
            var obj = database.GetData<SalesHeader>();
            if (obj != null) items = ConvertObservable<SalesHeader>(obj.Where(i => i.Status == "Released" && i.DocumentType == "SO" && i.SellToCustomer==custno)).ToList();
            return items;
        }

        public async Task<string> SaveSQLite_SalesHeader(SalesHeader record)
        {
            try
            {
                database.SaveData<SalesHeader>(record);
               return "Success";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public async Task<Dictionary<int,string>> SaveSQLite_Payment(Payment record)
        {
            Dictionary<int, string> dicResult = new Dictionary<int, string>();
            try
            {
                
               int retval= database.SaveData<Payment>(record);
                dicResult.Add(retval, "Success");
                return dicResult;
            }
            catch (Exception ex)
            {
                dicResult.Add(-1, ex.Message.ToString());
                return dicResult;
            }
        }

        public async Task<string> SaveSQLite_PaidReference(PaidReference record)
        {
            try
            {
                database.SaveData<PaidReference>(record);
                return "Success";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public string DeletePaidReferencebyDocNo(string docno)
        {
            string[] parameters = new string[] { docno };
            database.Query<PaidReference>("DELETE FROM PaidReference WHERE DocumentNo=?", parameters);
            return "Success";
        }

        public async Task<ObservableCollection<SalesHeader>> GetSQLite_SalesHeader()
        {
            ObservableCollection<SalesHeader> items = new ObservableCollection<SalesHeader>();
            var obj = database.GetData<SalesHeader>();
            if (obj != null) items = ConvertObservable<SalesHeader>(obj);
            return items;
        }

        public async Task <SalesHeader> GetSalesHeaderbyID(int id)
        {
            var Items = database.GetData<SalesHeader>();
            return Items.Where(i => i.ID == id).ToList().FirstOrDefault();
        }

        public async Task<SalesHeader> GetSalesHeaderbyDocNo(string docNo)
        {
            var Items = database.GetData<SalesHeader>();
            return Items.Where(i => i.DocumentNo == docNo).ToList().FirstOrDefault();
        }

        public Payment GetPaymentbyID(int id)
        {
            var Items = database.GetData<Payment>();
            return Items.Where(i => i.ID == id).ToList().FirstOrDefault();
        }

        public async Task<ObservableCollection<SalesHeader>> GetSQLite_SalesHeaderbyStatus(string status,string docType)
        {
            ObservableCollection<SalesHeader> items = new ObservableCollection<SalesHeader>();
            var obj = database.GetData<SalesHeader>();
            if (obj != null) items = ConvertObservable<SalesHeader>(obj.Where(i => i.Status == status && i.DocumentType==docType));
            return items;
        }

        public async Task<ObservableCollection<Payment>> GetSQLite_PaymentbyStatus(string status)
        {
            ObservableCollection<Payment> items = new ObservableCollection<Payment>();
            var obj = database.GetData<Payment>();
            if (obj != null) items = ConvertObservable<Payment>(obj.Where(i => i.RecStatus == status));
            return items;
        }
        //public List<Item> GetItemByNo(string itemno)
        //{
        //    List<Item> items = new List<Item>();
        //    var obj = database.GetData<Money>();
        //    if (items != null) items = obj.Where(i => i.ItemNo == itemno).ToList();
        //return headers.OrderBy(i => i.ID).LastOrDefault();
        //    return items.OrderBy(x => x.ItemNo).ToList();
        //}

        public ObservableCollection<T> ConvertObservable<T>(IEnumerable<T> original)
        {
            return new ObservableCollection<T>(original);
        }

        public async Task<List<NumberSeries>> GetSQLite_NumberSeries()
        {
            List<NumberSeries> items = new List<NumberSeries>();
            var obj = database.GetData<NumberSeries>();
            if (items != null) items = obj.ToList();
            return items;
        }

        public async Task<List<PaidReference>> GetSQLite_PaidReference()
        {
            List<PaidReference> items = new List<PaidReference>();
            var obj = database.GetData<PaidReference>();
            if (items != null) items = obj.ToList();
            return items;
        }

        public string SaveSQLite_NumberSeries(ObservableCollection<NumberSeries> numSeries)
        {
            try
            {
                database.DeleteAll<NumberSeries>();

                foreach (NumberSeries rec in numSeries)
                {
                    database.SaveData<NumberSeries>(rec);
                }
                return "Success";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public string GetLastNoSeries(string code)
        {
            try
            {
                string strLastNumCode = string.Empty;
                string strQuery = "SELECT * FROM NumberSeries WHERE Code=?";
                string[] parameters = new string[] { code };
                NumberSeries series = new NumberSeries();
                series = database.Query<NumberSeries>(strQuery, parameters).FirstOrDefault();
                if (series != null)
                {
                    int lastNumSeries = series.LastNoSeries + 1;
                    strLastNumCode = series.Code + "-" + lastNumSeries.ToString();

                }
                return strLastNumCode;
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public string IncreaseNoSeries(string code,string strLastNumCode)
        {
            try
            {
                int lastNumSeries;
                string strQuery = "SELECT * FROM NumberSeries WHERE Code=?";
                string[] parameters = new string[] { code };
                NumberSeries series = new NumberSeries();
                series = database.Query<NumberSeries>(strQuery, parameters).FirstOrDefault();
                if(series!=null)
                {
                    if(series.LastNoCode==strLastNumCode)
                    {
                        lastNumSeries = series.LastNoSeries;
                    }
                    else
                        lastNumSeries = series.LastNoSeries + 1;
                    strLastNumCode = series.Code + "-" + lastNumSeries.ToString();

                    NumberSeries newSeries = new NumberSeries() { ID = series.ID, Code = series.Code, LastNoCode = strLastNumCode, LastNoSeries = lastNumSeries, Increment = 1 };
                        database.SaveData<NumberSeries>(newSeries);

                }
                return strLastNumCode;
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public async Task<string> SaveSQLite_SalesLine(SalesLine record)
        {
            try
            {
                database.SaveData<SalesLine>(record);
                return "Success";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public async Task<string> SaveSQLite_DeviceInfo(DeviceInfo record)
        {
            try
            {
                database.DeleteAll<DeviceInfo>();
                database.SaveData<DeviceInfo>(record);
                return "Success";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }

        public async Task<ObservableCollection<SalesLine>> GetSQLite_SalesLine()
        {
            ObservableCollection<SalesLine> items = new ObservableCollection<SalesLine>();
            var obj = database.GetData<SalesLine>();
            if (obj != null) items = ConvertObservable<SalesLine>(obj);
            return items;
        }

        public async Task<SalesLine> GetSalesLinebyID(int id)
        {
            var Items = database.GetData<SalesLine>();
            return Items.Where(i => i.ID == id).ToList().FirstOrDefault();
        }

        public async Task<DeviceInfo> GetDeviceInfo()
        {
            var Items = database.GetData<DeviceInfo>();
            return Items.ToList().FirstOrDefault();
        }

        public async Task<Customer> GetCustomerbyCode(string code)
        {
            var Customers = database.GetData<Customer>();
            return Customers.Where(i => i.CustomerNo == code).ToList().FirstOrDefault();
        }

        public async Task<ObservableCollection<SalesLine>> GetSalesLinesbyDocNo(string docNo)
        {
            // var Items = database.GetData<SalesLine>();
            //if(App.Id!=0)
            // {
            ObservableCollection<SalesLine>  recSalesLine = new ObservableCollection<SalesLine>();
            recSalesLine.Clear();
            string[] parameters = new string[] { docNo };
            var Items = database.Query<SalesLine>("SELECT * FROM SalesLine WHERE DocumentNo=?", parameters);
            foreach (var item in Items)
            {
                recSalesLine.Add(item);
            }
            // }
            return recSalesLine;
        }

        public async Task<ObservableCollection<Customer>> GetCustomerbySalesPersonCode(string code)
        {
            // var Items = database.GetData<SalesLine>();
            //if(App.Id!=0)
            // {
            ObservableCollection<Customer> records = new ObservableCollection<Customer>();
            records.Clear();
            string[] parameters = new string[] { code };
            var Items = database.Query<Customer>("SELECT * FROM Customer WHERE SalesPersonCode=?", parameters);
            foreach (var item in Items)
            {
                records.Add(item);
            }
            // }
            return records;
        }

        public ObservableCollection<SalesLine> GetReleaseLinesbyDocNo(string docNo)
        {
            // var Items = database.GetData<SalesLine>();
            //if(App.Id!=0)
            // {
            ObservableCollection<SalesLine> recSalesLine = new ObservableCollection<SalesLine>();
            recSalesLine.Clear();
            string[] parameters = new string[] { docNo };
            var Items = database.Query<SalesLine>("SELECT * FROM SalesLine WHERE DocumentNo=?", parameters);
            foreach (var item in Items)
            {
                recSalesLine.Add(item);
            }
            // }
            return recSalesLine;
        }

        public async Task<ObservableCollection<ItemUOM>> GetItemUOMbyItemNo(string itemNo)
        {
            // var Items = database.GetData<SalesLine>();
            //if(App.Id!=0)
            // {
            ObservableCollection<ItemUOM> rec = new ObservableCollection<ItemUOM>();
            rec.Clear();
            string[] parameters = new string[] { itemNo };
            var Items = database.Query<ItemUOM>("SELECT * FROM ItemUOM WHERE ItemNo=?", parameters);
            foreach (var item in Items)
            {
                rec.Add(item);
            }
            // }
            return rec;
        }

        public List<SalesPrice> GetItemPricebyItemNo(string itemNo,string saleCode)
        {
            // var Items = database.GetData<SalesLine>();
            //if(App.Id!=0)
            // {
            List<SalesPrice> rec = new List<SalesPrice>();
            string[] parameters = new string[] { itemNo,saleCode};
            var Items = database.Query<SalesPrice>("SELECT * FROM SalesPrice WHERE ItemNo=? and CustomerNo=?", parameters);
            foreach (var item in Items)
            {
                rec.Add(item);
            }
            // }
            return rec;
        }

        public async Task<ObservableCollection<Payment>> GetPaymentbyStatus(string status)
        {
            // var Items = database.GetData<SalesLine>();
            //if(App.Id!=0)
            // {
            ObservableCollection<Payment> records = new ObservableCollection<Payment>();
            records.Clear();
            string[] parameters = new string[] { status };
            var Items = database.Query<Payment>("SELECT * FROM Payment WHERE RecStatus=?", parameters);
            foreach (var item in Items)
            {
                records.Add(item);
            }
            // }
            return records;
        }

        public ObservableCollection<Payment> GetPaymentOnDate(string ondate)
        {
            // var Items = database.GetData<SalesLine>();
            //if(App.Id!=0)
            // {
            ObservableCollection<Payment> records = new ObservableCollection<Payment>();
            records.Clear();
            string[] parameters = new string[] { ondate };
            var Items = database.Query<Payment>("SELECT * FROM Payment WHERE OnDate=?", parameters);
            foreach (var item in Items)
            {
                records.Add(item);
            }
            // }
            return records;
        }

        public string DeleteSalesLinebyDocNo(string docno)
        {
            string[] parameters = new string[] { docno };
            database.Query<SalesLine>("DELETE FROM SalesLine WHERE DocumentNo=?", parameters);
            return "Success";
        }

        public string DeleteSingleSalesLine(string docno,string itemno)
        {
            string[] parameters = new string[] { docno,itemno };
            database.Query<SalesLine>("DELETE FROM SalesLine WHERE DocumentNo=? AND ItemNo=?", parameters);
            return "Success";
        }

        public string UpdateSalesHeaderTotalAmount(decimal totalAmt, string docno)
        {
            string[] parameters = new string[] { totalAmt.ToString(), docno };
            database.Query<SalesHeader>("UPDATE SalesHeader SET TotalAmount=? WHERE DocumentNo=? ", parameters);
            return "Success";
        }
    }

    public class Database
    {
        static object locker = new object();

        ISQLite SQLite
        {
            get { return DependencyService.Get<ISQLite>(); }
        }

        readonly SQLiteConnection connection;
        readonly string DatabaseName;

        public Database(string databaseName)
        {
            DatabaseName = databaseName;
            connection = SQLite.GetConnection(DatabaseName);

            //  database = new SQLiteAsyncConnection(dbPath);
            //bool flag = Xamarin.Forms.DependencyService.Get<IFileHelper>().IsDbFileExist("DPDeliverySQLite.db3");
            // if (!flag) DatabaseInitialize();
        }

        public void DatabaseInitialize<T>()
        {
            lock (locker)
            {
                connection.DropTable<T>();
                connection.CreateTable<T>();
            }
        }
        public void CreateTable<T>()
        {
            lock (locker)
            {
                connection.CreateTable<T>();
            }
        }

        public long GetSize()
        {
            return SQLite.GetSize(DatabaseName);
        }

        public int SaveData<T>(T obj)
        {
            lock (locker)
            {
                var id = ((BaseItem)(Object)obj).ID;

                if (id != 0)
                {
                    connection.Update(obj);
                    return id;
                }
                else
                {
                    connection.Execute("PRAGMA synchronous = OFF");//THU test
                    connection.Insert(obj);
                    return connection.ExecuteScalar<int>("SELECT last_insert_rowid();");
                }
            }
        }

        public int SaveDataAll<T>(IEnumerable<T> objs)
        {
            lock (locker)
            {
                  connection.Execute("PRAGMA synchronous = OFF");//THU test
                  return  connection.InsertAll(objs);
            }
        }

        public int UpdateDataAll<T>(IEnumerable<T> objs)
        {
            lock (locker)
            {
                connection.Execute("PRAGMA synchronous = OFF");//THU test
                return  connection.UpdateAll(objs);
            }
        }

        public void ExecuteQuery(string query, object[] args)
        {
            lock (locker)
            {
                connection.Execute(query, args);
            }
        }


        public List<T> Query<T>(string query, object[] args) where T : new()
        {
            lock (locker)
            {
                return connection.Query<T>(query, args);
            }
        }

        public IEnumerable<T> GetData<T>() where T : new()
        {
            lock (locker)
            {
                return (from i in connection.Table<T>() select i).ToList();
            }
        }

        public int DeleteData<T>(int id)
        {
            lock (locker)
            {
                return connection.Delete<T>(id);
            }
        }

        public int DeleteAll<T>()
        {
            lock (locker)
            {
                return connection.DeleteAll<T>();
            }
        }
    }
}
