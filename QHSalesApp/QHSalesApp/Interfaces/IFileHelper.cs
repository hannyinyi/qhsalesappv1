﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QHSalesApp
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
        bool IsDbFileExist(string filename);
    }
}
