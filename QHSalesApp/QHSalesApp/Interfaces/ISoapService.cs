﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QHSalesApp
{
    public interface ISoapService
    {
        string DeviceRegistration(string strMobileAccessKey, string deviceId);
        string UserLogin(string mobileAccessKey, string deviceId, string userEmail, string password);
        DataTable GetLoginUsers(string mobileAccessKey);
        DataTable GetItems();
        DataTable GetItemUOMs();
        DataTable GetItemSalesPrices(string salesPersonCode);
        DataTable GetCustomers(string salesPersonCode);
        DataTable GetCustomerPriceHistory(string custno, string itemno);
        DataTable GetCustLedgerEntry(string custno);
        DataTable GetPaymentMethods();


        string ExportSalesHeader(string docNo, string selltocustomer, string selltoName, string billtoCustomer, string billtoName, string docDate, string status, string paymentMethod, decimal totalAmt, string doctype, string note, string strSingature);
        string ExportSalesLine(string docNo, string itemNo, string locCode, decimal qty, decimal focQty, string uom, decimal unitPrice, decimal lineDisPercent, decimal lineDisAmt, decimal lineAmt);
        string ExportPayment(string docNo, string onDate, string customerNo, string paymentMethod, decimal amount, string strSignature, string strImage, string salesPersonCode, string note, string recStatus, string refdocno, string sourcetype);
        DataTable CreateDataTable<T>(IEnumerable<T> list);
    }
}
